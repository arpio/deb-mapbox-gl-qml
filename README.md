# Unofficial Mapbox GL Native bindings for Qt QML

[![pipeline status](https://gitlab.com/arpio/deb-mapbox-gl-qml/badges/main/pipeline.svg)](https://gitlab.com/arpio/deb-mapbox-gl-qml/-/commits/main)

## Droidian Packaging

Packaging of [Unofficial Mapbox GL Native bindings for Qt QML](https://github.com/rinigus/mapbox-gl-qml) for [Droidian](https://droidian.org)